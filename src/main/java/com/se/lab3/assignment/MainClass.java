package com.se.lab3.assignment;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.Scanner;

public class MainClass {

	String jdbcUrl = "jdbc:mysql://localhost:3306/clientsales";
	String dbUsername = "root";
	String dbPassword = "root";

	Connection conn = null;
	Statement stmt = null;

	public static void main(String[] args) {

		MainClass mainClass = new MainClass();

		System.out.println("---Welcome---");

		System.out.println("Please slect any of below option:");
		System.out.println("1. Register");
		System.out.println("2. Login");
		System.out.println("3. Exit");

		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		while (input != null) {

			if (!input.equals("1") && !input.equals("2") && !input.equals("3")) {
				System.out.println("Invalid Input");
				input = scanner.nextLine();
				continue;
			}

			if (input.equals("3")) {
				System.exit(0);
			}

			System.out.println("Please provide login details:");
			System.out.println("Username:");
			String username = scanner.nextLine();
			System.out.println("Password:");
			String password = scanner.nextLine();

			mainClass.dbConnection();

			if (input.equals("1")) {
				String encryptedPwd = mainClass.encryptPwd(password);

				mainClass.insertQuery(username, encryptedPwd);

			} else if (input.equals("2")) {
				String encPwd = mainClass.selectQuery(username);

				boolean match = mainClass.decryptPwd(password, encPwd);
				if (match) {
					System.out.println("Login Successful");
				} else {
					System.out.println("Login Failed. Invalid Username or Password");
				}
			}
			System.out.println("Please slect any of below option:");
			System.out.println("1. Register");
			System.out.println("2. Login");

			input = scanner.nextLine();
			continue;
		}

		scanner.close();

	}

	public void dbConnection() {

		try {

			conn = DriverManager.getConnection(jdbcUrl, dbUsername, dbPassword);
			stmt = conn.createStatement();

			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS clientsales.login ("
					+ "UID int(10) unsigned NOT NULL AUTO_INCREMENT," + "USERNAME VARCHAR(255) NOT NULL,"
					+ "PASSWORD VARCHAR(255) NOT NULL," + "PRIMARY KEY (UID)," + "UNIQUE KEY `USERNAME` (`USERNAME`))"
					+ "ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// Close connection
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void insertQuery(String username, String password) {
		try {

			conn = DriverManager.getConnection(jdbcUrl, dbUsername, dbPassword);
			stmt = conn.createStatement();

			String sql = "INSERT INTO clientsales.login (USERNAME, PASSWORD) VALUES ('" + username + "','" + password
					+ "')";
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			System.out.println("Registration Successful");
		} catch (SQLIntegrityConstraintViolationException sql) {
			System.out.println("Username already exists");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// Close connection
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public String selectQuery(String username) {
		try {

			conn = DriverManager.getConnection(jdbcUrl, dbUsername, dbPassword);
			stmt = conn.createStatement();

			String sql = "Select PASSWORD from clientsales.login where USERNAME='" + username + "'";
			stmt = conn.createStatement();
			ResultSet resultset = stmt.executeQuery(sql);
			String password = null;
			while (resultset.next()) {
				password = resultset.getString("PASSWORD");
			}
			return password;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				// Close connection
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public String encryptPwd(String originalPassword) {
		return BCrypt.hashpw(originalPassword, BCrypt.gensalt(12));
	}

	public boolean decryptPwd(String originalPassword, String generatedSecuredPasswordHash) {
		return BCrypt.checkpw(originalPassword, generatedSecuredPasswordHash);
	}

}
